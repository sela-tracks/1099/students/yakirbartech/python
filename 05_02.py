string = input("Enter your string: ")

if string : 
    number_of_jump = input("Enter your number for every jump: ")
    
    if number_of_jump.isnumeric() :
        chart = input("Enter your special chart: ")

        if len(chart) == 1 :
            counter = 0
            new_string = ""

            for variable in string :
                counter += 1

                if counter % int(number_of_jump) != 0 :
                    new_string += variable
                else :
                    new_string += chart

            print(f"Your new string is: {new_string}")
        else :
            print("Sorry but your special chart must be 1 chart")
    else :
        print(f"Sorry but {number_of_jump} is not numeric")
else : 
    print("Sorry but your string is empty")